package com.orbitz.steps;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import org.hamcrest.Matchers;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.SendKeysAction;
import org.testng.annotations.Test;

import com.orbitz.pages.HomePage;
import com.orbitz.util.Utilities;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class HomePageSteps extends WebDriverBaseTestPage<WebDriverTestPage>{

	Actions action=new Actions(driver);
	@Override
	protected void openPage(PageLocator locator, Object... args) 
	{
		driver.get("/");
		driver.manage().window().maximize();
		
	}
	
	@QAFTestStep(description="i open orbitz homepage")
	public void launchApp()
	{

		openPage(null, null);
		waitForPageToLoad();
	}
	
	@QAFTestStep(description="i click on flights tab")
	public void clickOnFlightsTab()
	{
		click("tab.flights.homepage");
	}
	
	@QAFTestStep(description="i enter origin code {0} for {1}")
	public void enterFlightOrigin( String code, String city)
	{
		clear("tb.flightOrigin.homepage");
		clear("tb.flightDestination.homepage");
		sendKeys(code, "tb.flightOrigin.homepage");
		new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("div.airportSuggestion.homepage"),city))
							.waitForVisible(10000);
		action.sendKeys(Keys.ENTER).perform();
	}
	
	@QAFTestStep(description="i enter destination code {0} for {1}")
	public void enterFlightDestination( String code, String city)
	{
		clear("tb.flightDestination.homepage");
		sendKeys(code, "tb.flightDestination.homepage");
		new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("div.airportSuggestion.homepage"),city))
							.waitForVisible(10000);
		action.sendKeys(Keys.ENTER).perform();
	}
	
	@QAFTestStep(description="i enter departing date")
	public void enterDepartDate()
	{
		clear("tb.flightDepartDate.homepage");
		sendKeys(Utilities.getFormattedDate("MM/dd/YYYY", 1), "tb.flightDepartDate.homepage");
	}
	
	@QAFTestStep(description="i enter returning date")
	public void enterReturnDate()
	{
		clear("tb.flightReturnDate.homepage");
		sendKeys(Utilities.getFormattedDate("MM/dd/YYYY", 2), "tb.flightReturnDate.homepage");
	}
	
	@QAFTestStep(description="i click on search button")
	public void clickOnSearchButton()
	{
		click("btn.search.homepage");
	}
	
	
	

}
