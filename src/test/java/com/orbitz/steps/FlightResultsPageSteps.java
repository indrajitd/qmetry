package com.orbitz.steps;

import java.util.LinkedList;
import java.util.List;

import org.hamcrest.Matchers;

import com.orbitz.components.FlightDetailsComponent;
import com.orbitz.pages.HomePage;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.report.Report;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class FlightResultsPageSteps extends WebDriverBaseTestPage<HomePageSteps>{

	@FindBy(locator="li.flightmodule.flightdetails")
	private List<FlightDetailsComponent> components;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	
	public void launchPage()
	{
		openPage(null, null);
	}
	
	@QAFTestStep(description="flight results page should be dislayed with airports {0}")
	public void validateFlightsPageTitle(String airports)
	{
		Validator.assertThat(driver.getTitle(), Matchers.containsString("Flights | Orbitz"));
		Reporter.log("title matches");
		
		System.out.println("Size : " + components.size());
		
		for(int i=0;i<=6;i++)
		{
			Validator.assertThat(components.get(i).getAirports().getText(), Matchers.is(airports));
			Reporter.log("airport matches");
		}
					
	}

}
