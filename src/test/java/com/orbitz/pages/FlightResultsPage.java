package com.orbitz.pages;

import java.util.List;

import com.orbitz.components.FlightDetailsComponent;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;

public class FlightResultsPage extends WebDriverBaseTestPage<HomePage> {

	@FindBy(locator="li.flightmodule.flightdetails")
	private List<FlightDetailsComponent> flightDets;
	
	public List<FlightDetailsComponent> getFlightDets() {
		return flightDets;
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("");
		
	}
	
	
}
