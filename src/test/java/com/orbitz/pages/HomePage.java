package com.orbitz.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.selenium.WaitService;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.thoughtworks.selenium.webdriven.commands.WaitForCondition;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	private Actions action;
	
	@FindBy(locator="tab.flights.homepage")
	private QAFWebElement flightsTab;
		
	@FindBy(locator="tb.flightOrigin.homepage")
	private QAFWebElement flightOriginTB;
	
	@FindBy(locator="div.airportSuggestionPune.homepage")
	private QAFWebElement airportSuggestionPune;
	
	@FindBy(locator="tb.flightDestination.homepage")
	private QAFWebElement flightDestinationTB;
	
	@FindBy(locator="div.airportSuggestionKolkata.homepage")
	private QAFWebElement airportSuggestionKolkata;
	
	@FindBy(locator="tb.flightDepartDate.homepage")
	private QAFWebElement flightDepartDateTB;
	
	@FindBy(locator="tb.flightReturnDate.homepage")
	private QAFWebElement flightReturnDateTB;
	
	@FindBy(locator="btn.search.homepage")
	private QAFWebElement searchButton;
	
	@Override
	public void openPage(PageLocator pageLocator, Object... args)
	{
		driver.get("/");
		action=new Actions(driver);
	}
	
	public void clickOnFlightsTab()
	{
		flightsTab.click();
	}
	
	public void enterFlightOrigin(String originCode)
	{
		flightOriginTB.click();
		flightOriginTB.sendKeys(originCode);
	}
	
	public void selectSuggestionPune()
	{
		airportSuggestionPune.waitForPresent();
		action.sendKeys(Keys.ENTER).perform();
		
		
	}
	
	public void enterFlightDestination(String destinationCode)
	{
		flightDestinationTB.click();
		flightDestinationTB.sendKeys(destinationCode);
	}
	
	public void selectSuggestionKolkata()
	{
		airportSuggestionKolkata.waitForPresent();
		action.sendKeys(Keys.ENTER).perform();
	}
	
	public void enterFlightDepartDate(String departDate)
	{
		flightDepartDateTB.clear();
		flightDepartDateTB.sendKeys(departDate);
	}
	
	public void enterFlightReturnDate(String returnDate)
	{
		flightReturnDateTB.clear();
		flightReturnDateTB.sendKeys(returnDate);
	}
	
	public void clickOnSearch()
	{
		searchButton.click();
	}
	

}
