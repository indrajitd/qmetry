package com.orbitz.tests;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.orbitz.components.FlightDetailsComponent;
import com.orbitz.pages.FlightResultsPage;
import com.orbitz.pages.HomePage;
import com.orbitz.util.Utilities;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Validator;
import com.thoughtworks.selenium.ScreenshotListener;

import ch.qos.logback.core.boolex.Matcher;

public class PuneToKolkata  extends WebDriverTestCase
{

	@Test(groups={"SMOKE"},description="yo",invocationCount=1)
	public void scene() throws Exception{
		HomePage hp=new HomePage();
		FlightResultsPage frp=new FlightResultsPage();
		hp.openPage(null, null);
		hp.waitForPageToLoad();
		hp.clickOnFlightsTab();
		hp.enterFlightOrigin("pnq");
		hp.selectSuggestionPune();
		hp.enterFlightDestination("ccu");
		hp.selectSuggestionKolkata();
		hp.enterFlightDepartDate(Utilities.getFormattedDate("MM/dd/YYYY",1));
		hp.enterFlightReturnDate(Utilities.getFormattedDate("MM/dd/YYYY",2));
		hp.clickOnSearch();
		
		Validator.assertThat(getDriver().getTitle(), Matchers.containsString("Flights | Orbitz"));
		
		for(int i=0;i<=6;i++)
			Validator.assertThat(frp.getFlightDets().get(i).getAirports().getText(), Matchers.is("PNQ - CCU"));
	}





//
//	@QAFTestStep(description="don't know")
//	public void openMaybe(){
//	}


}
