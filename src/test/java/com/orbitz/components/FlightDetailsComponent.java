package com.orbitz.components;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FlightDetailsComponent extends QAFWebComponent{

	public FlightDetailsComponent(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(locator="div.airports.flightdetails")
	private QAFWebElement airports;
	
	@FindBy(locator="span.price.flightdetails")
	private QAFWebElement price;

	public QAFWebElement getPrice() {
		return price;
	}

	public QAFWebElement getAirports() {
		return airports;
	}
	
	

}
