package dataTest;

import java.util.List;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.orbitz.components.FlightDetailsComponent;
import com.orbitz.steps.FlightResultsPageSteps;
import com.orbitz.steps.HomePageSteps;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ReturnFlightTest extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator="li.flightmodule.flightdetails")
	private List<FlightDetailsComponent> components;
	
	@QAFDataProvider(dataFile="./resources/data/airports.csv")
	@Test(groups={"SMOKE"},description="data driven flights")
	public void testReturnFlight(String origin, String originCode, String dest, String destCode)
	{
		HomePageSteps hps=new HomePageSteps();
		hps.launchApp();
		hps.clickOnFlightsTab();
		hps.enterFlightOrigin(originCode, origin);
		hps.enterFlightDestination(destCode, dest);
		hps.enterDepartDate();
		hps.enterReturnDate();
		hps.clickOnSearchButton();
		
		Validator.assertThat(driver.getTitle(), Matchers.containsString("Flights | Orbitz"));
		Reporter.log("title matches");
		
		System.out.println("Size : " + components.size());
		
		for(int i=0;i<=6;i++)
		{
			Validator.assertThat(components.get(i).getAirports().getText(), Matchers.is(originCode+" - "+destCode));
			Reporter.log("airport matches");
		}
	}



	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}}
